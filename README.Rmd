---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# squirrels

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrels is to ...
Package relié au git https://gitlab.com/form_inter_certif2_oct2023/nadine/squirrels.git

Ce package a été créé dans le cadre d’une formation N2


## Installation

You can install the development version of squirrels like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

```{r example}
library(squirrels)
## basic example code
get_message_fur_color("Black")
```

## code of conduct

Please note that the {fusen} project is released with a [Contributor Code of Conduct](https://contributor-covenant.org/version/2/0/CODE_OF_CONDUCT.html). By contributing to this project, you agree to abide by its terms.

